﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartScene : MonoBehaviour
{
    public Text BestScoreText;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("BestScore"))
            BestScoreText.text = "Best Score: " + PlayerPrefs.GetInt("BestScore").ToString();
        else
            BestScoreText.text = "BestScore: 0";
    }

    public void StarGame()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
