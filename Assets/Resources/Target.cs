﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public int Value = 10;
    public float MoveSpeed = 0.05f;
    public bool IsMoveToLeft = false;
    public bool IsAMalus = false;
    public float LifeSpan = 6f;

    private GameManager GM;

    public void Start()
    {
        GM = FindObjectOfType<GameManager>();
        if (IsMoveToLeft)
            MoveSpeed *= -1;
        InvokeRepeating("Move", 0f, 0.01f);
        Invoke("AutoDestroy", LifeSpan);
    }

    public void OnMouseDown()
    {
        GM.AddToScore(Value);
        if (IsAMalus)
            GM.LoosALife();
        Destroy(this.gameObject);
    }

    private void Move()
    {
        transform.position = transform.position + new Vector3(MoveSpeed, 0f, 0f);
    }

    private void AutoDestroy()
    {
        Destroy(this.gameObject);
    }
}
