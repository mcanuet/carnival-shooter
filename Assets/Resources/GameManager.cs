﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("UI")]
    [SerializeField]
    private Text Timer;
    [SerializeField]
    private GameObject[] LifePointsSprites;

    [SerializeField]
    private Text ScoreText;

    [Header("FX")]
    [SerializeField]
    private GameObject OnHitEffect;

    private int ScoreValue = 0;
    private float RemainingTime = 120f;
    private int LifePoints = 3;

    public void Start()
    {
        InvokeRepeating("DecreaseTimer", 1f, 1f);
    }

    public void AddToScore(int value)
    {
        ScoreValue += value;
        ScoreText.text = "Score: " + ScoreValue.ToString();
        Transform SpawnTransform = transform;
        SpawnTransform.position = Vector3.zero;
        SpawnTransform.rotation = Quaternion.identity;
        SpawnTransform.localScale = Vector3.one;
        SpawnTransform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0f);
        Instantiate<GameObject>(OnHitEffect, SpawnTransform);
    }

    public void LoosALife()
    {
        LifePoints -= 1;
        LifePointsSprites[LifePoints].SetActive(false);
        if (LifePoints == 0)
            EndGame();
    }

    private void DecreaseTimer()
    {
        RemainingTime -= 1f;
        Timer.text = RemainingTime.ToString();
        if (RemainingTime <= 0)
            EndGame();
    }

    private void EndGame()
    {
        CancelInvoke();
        if (PlayerPrefs.HasKey("BestScore"))
        {
            if (PlayerPrefs.GetInt("BestScore") < ScoreValue)
                PlayerPrefs.SetInt("BestScore", ScoreValue);
        }
        else
            PlayerPrefs.SetInt("BestScore", ScoreValue);
        SceneManager.LoadScene(0);
    }
}
