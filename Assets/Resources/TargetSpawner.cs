﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSpawner : MonoBehaviour
{
    [Header("Targets")]
    public bool TargetGoToLeft = false;
    public float TargetSpeed = 0.05f;

    [Header("Spawner")]
    public GameObject TargetSample;
    public float spawnRate = 0.8f;
    public float SpawnDecal = 1f;

    void Start()
    {
        InvokeRepeating("SpawnTarget", SpawnDecal, spawnRate);
    }

    private void SpawnTarget()
    {
        float rnd = Random.value;
        GameObject target = Instantiate<GameObject>(TargetSample, transform);
        target.GetComponent<Target>().IsMoveToLeft = TargetGoToLeft;
        target.GetComponent<Target>().MoveSpeed = TargetSpeed;
        if (rnd < 0.3)
        {
            target.GetComponent<Target>().Value = -10;
            target.GetComponent<Target>().IsAMalus = true;
            target.GetComponentInChildren<SpriteRenderer>().color = Color.black;
        }
        else if (rnd >= 0.3 && rnd < 0.7)
        {
            target.GetComponent<Target>().Value = 10;
            target.GetComponentInChildren<SpriteRenderer>().color = Color.green;
        }
        else if (rnd >= 0.7 && rnd < 0.9)
        {
            target.GetComponent<Target>().Value = 20;
            target.GetComponentInChildren<SpriteRenderer>().color = Color.blue;
        }
        else if (rnd >= 0.9 && rnd <= 1)
        {
            target.GetComponent<Target>().Value = 30;
            target.GetComponentInChildren<SpriteRenderer>().color = Color.red;
        }
    }
 }
